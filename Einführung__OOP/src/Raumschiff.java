import java.util.ArrayList;
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	private Ladung neueLadung;
	private Raumschiff r;
	private String message;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	

public Raumschiff( int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.energieversorgungInProzent = energieversorgungInProzent;
	this.schildeInProzent = schildeInProzent;
	this.huelleInProzent = huelleInProzent;
	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	this.schiffsname = schiffsname;
	this.androidenAnzahl = androidenAnzahl;
}
	

public int getPhotonentorpedoAnzahl() {
	return photonentorpedoAnzahl;
}
public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
}
public int getEnergieversorgungInProzent() {
	return energieversorgungInProzent;
}
public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
	this.energieversorgungInProzent = energieversorgungInProzent;
}
public int getSchildeInProzent() {
	return schildeInProzent;
}
public void setSchildeInProzent(int schildeInProzent) {
	this.schildeInProzent = schildeInProzent;
}
public int getHuelleInProzent() {
	return huelleInProzent;
}
public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}
public int getLebenserhaltungssystemeInProzent() {
	return lebenserhaltungssystemeInProzent;
}
public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
}
public int getAndroidenAnzahl() {
	return androidenAnzahl;
}
public void setAndroidenAnzahl(int androidenAnzahl) {
	this.androidenAnzahl = androidenAnzahl;
}
public String getSchiffsname() {
	return schiffsname;
}
public void setSchiffsname(String schiffsname) {
	this.schiffsname = schiffsname;
}
public void addLadung (Ladung neueLadung) {
	this.ladungsverzeichnis.add(neueLadung);
}

	

/**
 * Die Methode photonentorpedoSchiessen schie�t Photonentorpedos ab und sendet bei nichtausreichender Ladung die Nachricht Click an alle.
 * @param r
 */

public void photonentorpedoSchiessen (Raumschiff r) {
	this.r = r;
	if (photonentorpedoAnzahl >= 0) 
	{
		nachrichtAnAlle("-=*Click*=-");
	}
	else 
	{
		photonentorpedoAnzahl =- 1;
	
		treffer(r);
		nachrichtAnAlle("Photonentorpedo abgeschossen!");
	}
}

 /**
  * Die Methode phaserkanoneSchiessen schie�t eine Phaserkanone ab und sendet bei nichtausreichender Ladung die Nachricht Click an alle.
  * @param r
  */
public void phaserkanoneSchiessen (Raumschiff r) {
	this.r = r;
	if (energieversorgungInProzent < 50) 
	{
		nachrichtAnAlle("-=*Click*=-");
		}
	else 
	{
		energieversorgungInProzent =- 50;
		
		treffer(r);
		nachrichtAnAlle("\nPhaserkanone abgeschossen!");
	}
}
/**
 * Die Methode treffer sendet eine Nachricht an alle, welche angibt, dass und welches Raumschiff getroffen wurde.
 * @param r
 */
private void treffer (Raumschiff r) {
	this.r = r;
	nachrichtAnAlle(r.schiffsname + " wurde getroffen!");

	}
	
/**
 * Die Methode nachrichtAnAlle f�gt eine Nachricht zur Array List broadcastKommunikator hinzu.
 * @param message
 */
public void nachrichtAnAlle (String message) {
	broadcastKommunikator.add(message);
	System.out.println(broadcastKommunikator);
}
public ArrayList<String> eintraegeLogbuchZurueckgeben = new ArrayList<String>();
private int anzahlTorpedos;
private Object schutzschilde;
private boolean energieversorgung;
private boolean schiffshuelle;
private int anzahlDroiden;

/**
 * Die Methode photonentorpedosLaden l�dt eine bestimmte Anzahl Torpedos in das Raumschiff.
 * @param anzahlTorpedos
 */
public void photonentorpedosLaden (int anzahlTorpedos) {
	this.anzahlTorpedos = anzahlTorpedos;
}

/**
 * Die Methode reparaturDurchfuehren setzt alle gegebenen Parameter auf den gew�nschten Wert.
 * @param schulzschilde
 * @param energieversorgung
 * @param schiffshuelle
 * @param anzahlDroiden
 */
public void reparaturDurchfuehren (boolean schulzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
	this.schutzschilde = schutzschilde;
	this.energieversorgung = energieversorgung;
	this.schiffshuelle = schiffshuelle;
	this.anzahlDroiden = anzahlDroiden;
	
}
/**
 * Die Methode zustandRaumschiff gibt den Schiffsnamen und die Statistik des Schiffs wieder.
 */
public void zustandRaumschiff() {
	System.out.println("Schiffsname: " + schiffsname);
	System.out.printf("\nAnzahl der Photonentorpedos: %d \nEnergieversorgung in Prozent: %d \nSchild in Prozent: %d \nH�lle in Prozent: %d \nLebenserhaltungssysteme in Prozent: %d \nAndroidenanzahl: %d ", photonentorpedoAnzahl, energieversorgungInProzent, schildeInProzent, huelleInProzent, lebenserhaltungssystemeInProzent, androidenAnzahl);
}
/**
 * Die Methode ladungsverzeichnisAusgeben gibt das Ladungsverzeichnis in der Konsole aus.
 */
public void ladungsverzeichnisAusgeben () {
		System.out.println(this.ladungsverzeichnis);
}
/**
 * Die Methode logbuchEintraegeZurueckgeben gibt die Nachrichten, welche an den broadcastKommunikator geschickt wurden, in der Konsole aus.
 */
public void logbuchEintraegeZurueckgeben () {
	System.out.println(broadcastKommunikator.toString());
}
/**
 * Die Methode ladungsverzeichnisAufraeumen leert das Ladungsverzeichnis.
 *
 */
public void ladungsverzeichnisAufraeumen() {
	for (int i = 0; i < ladungsverzeichnis.size(); i++) {
		if (ladungsverzeichnis.get(i).getAnzahl() == 0) {
			ladungsverzeichnis.remove(i);
		}
	}
}

	
/**
 * Die Methode trefferVermerken gibt an, wie viel bei einem Treffer abgezogen wird und wann das Raumschiff kaputt ist.
 * @param r
 */
public void trefferVermerken (Raumschiff r) {
	schildeInProzent =- 50;
	if (schildeInProzent <= 0)
	{
		huelleInProzent =- 50;
		energieversorgungInProzent =- 50;
		if (huelleInProzent <= 0)
		{
			lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle("Die Lebenserhaltungssysteme von " + r.schiffsname + " wurden vernichtet.");
		}
	}
}



	
	
}