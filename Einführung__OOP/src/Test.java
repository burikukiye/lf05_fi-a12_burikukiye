
public class Test {

	public static void main(String[] args) {
		Raumschiff klingone = new Raumschiff (1,100,100,100,100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff (2, 100, 100, 1000, 100, 2, "IRA Khazara");
		Raumschiff vulkanier = new Raumschiff (0, 80, 80, 50, 100, 5, "Ni'Var");
		// TODO Auto-generated method stub
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonenschwert", 200);
		Ladung l6 = new Ladung("Plasmawaffe", 50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		klingone.addLadung(l1);
		klingone.addLadung(l5);
		romulaner.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l6);
		vulkanier.addLadung(l4);
		vulkanier.addLadung(l7);
		
		
		
		klingone.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingone);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch!");
		klingone.zustandRaumschiff();
		klingone.ladungsverzeichnisAusgeben();
		klingone.photonentorpedoSchiessen(romulaner);
		klingone.photonentorpedoSchiessen(romulaner);
		klingone.ladungsverzeichnisAusgeben();
		klingone.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.logbuchEintraegeZurueckgeben();
		romulaner.logbuchEintraegeZurueckgeben();
		klingone.logbuchEintraegeZurueckgeben();
		}
	}


