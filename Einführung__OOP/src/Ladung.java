import java.util.ArrayList;
public class Ladung {
	private String art;
	private int anzahl;
	
	public Ladung(String art, int anzahl) {
		this.art = art;
		this.anzahl = anzahl;
		
	}
	public String getArt() {
		return art;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	public void setArt(String art) {
		this.art = art;
	}
	@Override
	public String toString() {
		return  "Ladung: " + this.art + ": " + this.anzahl;
	}
	
	
}
