import javax.swing.*;  
    import java.awt.event.*;  
    public class orderFood extends JFrame implements ActionListener{  
        JLabel l1, l2, l3, l4, l5;  
        JCheckBox cb0, cb1,cb2,cb3;  
        JButton b;  
        orderFood(){  
            l1=new JLabel("Willkommen im Restaurant\n\nVorspeisen:");  
            l1.setBounds(50,50,300,20); 
            cb0=new JCheckBox("Frühlingsrollen");  
            cb0.setBounds(100,50,300,20);
            cb1=new JCheckBox("Wantan m. Fleisch");  
            cb1.setBounds(100,100,150,20);  
            cb2=new JCheckBox("Wantan vegetarisch");  
            cb2.setBounds(100,150,150,20);  
            cb3=new JCheckBox("Tea @ 10");  
            cb3.setBounds(100,200,150,20);  
            b=new JButton("Order");  
            b.setBounds(100,250,80,30);  
            b.addActionListener(this);  
            add(l1);add(cb0);add(cb1);add(cb2);add(cb3);add(b);  
            setSize(400,400);  
            setLayout(null);  
            setVisible(true);  
            setDefaultCloseOperation(EXIT_ON_CLOSE);  
        }  
        public void actionPerformed(ActionEvent e){  
            float amount=0;  
            String msg="";  
            if(cb0.isSelected()) {
            	amount+=3.50;  
                msg="Frühlingsrollen: 3,50€\n";
            }
            if(cb1.isSelected()){  
                amount+=3.50;  
                msg="Wantan m. Fleisch: 3,50€\n";  
            }  
            if(cb2.isSelected()){  
                amount+=2.00;  
                msg+="Wantan m. Fleisch: 2€\n";  
            }  
            if(cb3.isSelected()){  
                amount+=10;  
                msg+="Tea: 10\n";  
            }  
            msg+="-----------------\n";  
            JOptionPane.showMessageDialog(this,msg+"Total: "+amount);  
        }  
        public static void main(String[] args) {  
            new orderFood();  
        }  
    }  