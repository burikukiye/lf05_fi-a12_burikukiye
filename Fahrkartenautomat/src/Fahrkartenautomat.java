﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {

      boolean ticketKauf = true;
      while (ticketKauf == true) {
       double zuZahlen = fahrkartenbestellungErfassen(); 
       double rueckgabeBetrag = fahrkartenBezahlen(zuZahlen);
       fahrkartenAusgeben();
       rueckgeldAusgeben(rueckgabeBetrag);
      }
    }   
    
    public static double fahrkartenbestellungErfassen () {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("\nSchönen guten Tag, bitte geben Sie hier die Anzahl der gewünschten Tickets ein: ");
    	double anzahlTickets = tastatur.nextDouble();
  
    	
    	boolean groesserEins = anzahlTickets >=1 && anzahlTickets <= 10;
    	double aufrechnen = 0;
 
    	
    	while (!groesserEins) {
    		System.out.println("\nBitte geben Sie eine Anzahl von 1 bis 10 Tickets an!");
    		anzahlTickets = tastatur.nextDouble();
    		groesserEins = anzahlTickets >=1 && anzahlTickets <= 10;
    	}
    	for (int i = 0; i <= anzahlTickets - 1; i++) {
    		
    	System.out.println("\nWählen Sie Ihre Wunschfahrkarte für Berlin AB aus: \nEinzelfahrschein Regeltarif AB [2,90 EUR] (1) \nTageskarte Regeltarif AB [8,60 EUR] (2) \nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	double zuZahlenderBetrag = tastatur.nextDouble();
    	boolean negativ = zuZahlenderBetrag > 3;
    	while (negativ) {
    		System.out.println("\nUngültige Eingabe! Bitte wählen Sie eines der vorgeschlagenen Tickets aus!");
    		zuZahlenderBetrag = tastatur.nextDouble();
    		negativ = false;
    	}
    	
    		if (zuZahlenderBetrag == 1) {
    			zuZahlenderBetrag = 2.90;
    			aufrechnen += zuZahlenderBetrag;
    			System.out.printf("\nDie Zwischensumme beträgt %.2f Euro", aufrechnen);
    			negativ = false;
    			
    		}
    		else if (zuZahlenderBetrag == 2) {
    			zuZahlenderBetrag = 8.60;
    			aufrechnen += zuZahlenderBetrag;
    			System.out.printf("\nDie Zwischensumme beträgt %.2f Euro", aufrechnen);
    			negativ = false;
    			
    		}
    		else if (zuZahlenderBetrag == 3) {
    			zuZahlenderBetrag = 23.50;
    			aufrechnen += zuZahlenderBetrag;
    			System.out.printf("\nDie Zwischensumme beträgt %.2f Euro", aufrechnen);
    			negativ = false;
    			
    		}
    		else {
    			negativ = zuZahlenderBetrag > 3;	
    		}
    		
    	}
    	
    	
    	
    	
//    	double zuZahlen = anzahlTickets * zuZahlenderBetrag;
    	double zuZahlen = aufrechnen;
    	System.out.println("\nDer Gesamtpreis für " + anzahlTickets + "Tickets beträgt: " + zuZahlen);
    	return zuZahlen;
    }
    
    
    public static double fahrkartenBezahlen (double zuZahlen) {
   
    	Scanner tastatur3 = new Scanner(System.in);
        double eingezahlt = 0.00;
        
        while(eingezahlt < zuZahlen)
        {
           double nochZuZahlen = zuZahlen - eingezahlt;
           double eingeworfeneMuenze = tastatur3.nextDouble();
           eingezahlt += eingeworfeneMuenze;
           if (nochZuZahlen - eingeworfeneMuenze > 0) {
        	   System.out.printf("\nNoch zu zahlen: %.2f Euro", (nochZuZahlen - eingeworfeneMuenze));
        	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
        	   
        		   
     	   
           }
     	 
        }
        double rueckgabeBetrag  = eingezahlt - zuZahlen;
        return rueckgabeBetrag;
    	
    }
    
    
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            
         }
         System.out.println("\n\n");
  		} 
  

   
    
    public static void rueckgeldAusgeben (double rückgabeBetrag) {
    	
        if(rückgabeBetrag > 0.0)
        {
     	   System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f Euro", rückgabeBetrag);
     	   System.out.println("\nwird in folgenden Münzen ausgezahlt: ");

            while(rückgabeBetrag > 1.99) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabeBetrag -= 2.0;
            }
            while(rückgabeBetrag > 0.99) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabeBetrag -= 1.0;
            }
            while(rückgabeBetrag > 0.49) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabeBetrag -= 0.5;
            }
            while(rückgabeBetrag > 0.19) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabeBetrag -= 0.2;
            }
            while(rückgabeBetrag > 0.09) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabeBetrag -= 0.1;
            }
            while(rückgabeBetrag > 0.04)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabeBetrag -= 0.05;
            }
            System.out.println("Vergessen Sie nicht den Fahrschein vor Fahrtantritt zu entwerten. \nWir wünschen eine schöne Fahrt!");
}
        
    }
    }